from django.contrib import admin
from django.urls import path
from django.conf.urls import include


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('apps.core.urls')),
    path('', include('apps.autenticacao.urls')),
    path('empresa/', include('apps.fornecedor.urls')),
    path('', include('apps.estoque.urls')),
    path('material/', include('apps.material.urls')),
    path('cliente/', include('apps.cliente.urls')),


]
