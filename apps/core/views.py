from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.db.models import Sum
from django.http import JsonResponse

from django.shortcuts import render
from pandas._libs import json

from apps.estoque.models import EstoqueSaida, EstoqueEntrada, ItensEstoque, Estoque
from apps.material.models import MaterialEstoque
from operator import itemgetter

@login_required
def index(request):
    pg_pend = Estoque.objects.all()
    pgp = 0
    entp = 0
    for pend in pg_pend:
        if pend.situacao == 'pend':
            pgp += 1
        if pend.entrega and pend.status_entrega != 'f':
            entp += 1
    return render(request, 'dashboard.html', {'pg_pend': pgp, 'entrega_pend': entp})


@login_required()
def api_venda(request):

    estoquesai = EstoqueSaida.objects.filter(movimento='s', data__month__in=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
    estoqueent = EstoqueEntrada.objects.filter(movimento='e', data__month__in=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
    itens_entrada = ItensEstoque.objects.filter(estoque__movimento='e')
    itens_saida = ItensEstoque.objects.filter(estoque__movimento='s')
    itement = sum([float(obj.quantidade) for obj in itens_entrada])
    itemsai = sum([float(obj.quantidade) for obj in itens_saida])
    totsai = sum([float(obj.valor_total) for obj in estoquesai if obj.data.year == datetime.today().year])
    totent = sum([float(obj.valor_total) for obj in estoqueent if obj.data.year == datetime.today().year])
    totmes = sum([float(obj.valor_total) for obj in estoquesai if obj.data.month == datetime.today().month])
    lucro = totsai - totent

    mat = MaterialEstoque.objects.all()

    item = {}
    # loop no models MaterialEstoque para obter todos os materias
    for m in mat:
        # Guardando o id de todos os materias na variavel mat_id
        mat_id = m.id
        # Guardando o nome de todos os materias na variavel mat_nome
        mat_nome = m.nome
        # Filtrando todos os materiais de saida e somando o valor de cada item
        ite = ItensEstoque.objects.filter(
            material_id=mat_id, estoque__movimento='s',
            estoque__data__year=datetime.today().year).aggregate(Sum('valor'))['valor__sum']
        # Adicionando o valor ao nome do material e incluindo no dicionario item
        if ite == None:
            ite = 0
        item[mat_nome] = ite

    # Ordenando elementos do dicionario item de forma decrescente( os 5 com valores maiores )
    # e fatiando em apenas 5 elementos
    i = sorted(item.items(), key=itemgetter(1), reverse=True)[:5]

    # Consulta valores de venda por mes do ano atual
    totjan = sum([float(obj.valor_total) for obj in estoquesai if obj.data.month == 1 if obj.data.year == datetime.today().year])
    totfev = sum([float(obj.valor_total) for obj in estoquesai if obj.data.month == 2 if obj.data.year == datetime.today().year])
    totmar = sum([float(obj.valor_total) for obj in estoquesai if obj.data.month == 3 if obj.data.year == datetime.today().year])
    totabr = sum([float(obj.valor_total) for obj in estoquesai if obj.data.month == 4 if obj.data.year == datetime.today().year])
    totmai = sum([float(obj.valor_total) for obj in estoquesai if obj.data.month == 5 if obj.data.year == datetime.today().year])
    totjun = sum([float(obj.valor_total) for obj in estoquesai if obj.data.month == 6 if obj.data.year == datetime.today().year])
    totjul = sum([float(obj.valor_total) for obj in estoquesai if obj.data.month == 7 if obj.data.year == datetime.today().year])
    totago = sum([float(obj.valor_total) for obj in estoquesai if obj.data.month == 8 if obj.data.year == datetime.today().year])
    totset = sum([float(obj.valor_total) for obj in estoquesai if obj.data.month == 9 if obj.data.year == datetime.today().year])
    totout = sum([float(obj.valor_total) for obj in estoquesai if obj.data.month == 10 if obj.data.year == datetime.today().year])
    totnov = sum([float(obj.valor_total) for obj in estoquesai if obj.data.month == 11 if obj.data.year == datetime.today().year])
    totdez = sum([float(obj.valor_total) for obj in estoquesai if obj.data.month == 12 if obj.data.year == datetime.today().year])

    # Convertendo os valores em formato json para ser consumido no front atraves de uma função ajax
    context = {
        'tot': json.dumps(totsai),
        'totent': json.dumps(totent),
        'lucro': json.dumps(lucro),
        'totmes': json.dumps(totmes),
        'jan': json.dumps(totjan),
        'fev': json.dumps(totfev),
        'mar': json.dumps(totmar),
        'abr': json.dumps(totabr),
        'mai': json.dumps(totmai),
        'jun': json.dumps(totjun),
        'jul': json.dumps(totjul),
        'ago': json.dumps(totago),
        'set': json.dumps(totset),
        'out': json.dumps(totout),
        'nov': json.dumps(totnov),
        'dez': json.dumps(totdez),
        'item': dict(i),
        'quant_entrada': json.dumps(itement),
        'quant_saida': json.dumps(itemsai)
    }
    return JsonResponse(context)