from django.urls import path

from apps.core.views import *


urlpatterns = [
    path('', index, name='index'),
    path('api/venda/', api_venda, name='api-venda'),
]