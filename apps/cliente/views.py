from django.contrib.auth.decorators import login_required
from django.shortcuts import render

# Create your views here.
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, ListView, UpdateView

from apps.cliente.forms import ClienteForm
from apps.cliente.models import Cliente


@method_decorator([login_required], name='dispatch')
class CreateCliente(CreateView):
    model = Cliente
    form_class = ClienteForm
    template_name = 'cadastrar_cliente.html'


@method_decorator([login_required], name='dispatch')
class ListCliente(ListView):
    model = Cliente
    template_name = 'listar_cliente.html'
    context_object_name = 'object_list'
    paginate_by = 10


@method_decorator([login_required], name='dispatch')
class UpdateCliente(UpdateView):
    model = Cliente
    form_class = ClienteForm
    template_name = 'cadastrar_cliente.html'

    def get_object(self, queryset=None):
        return Cliente.objects.get(pk=self.kwargs['pk'])