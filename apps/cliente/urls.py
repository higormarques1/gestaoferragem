from django.urls import path

from apps.cliente.views import CreateCliente, ListCliente, UpdateCliente

urlpatterns = [
    path('cadastrar/', CreateCliente.as_view(), name='cadastrar_cliente'),
    path('listar/', ListCliente.as_view(), name='listar_cliente'),
    path('atualizar/<int:pk>/', UpdateCliente.as_view(), name='atualizar_cliente'),
]