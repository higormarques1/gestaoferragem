from django.db import models
from django.template.loader import render_to_string
from django.urls import reverse
from django.core.mail import send_mail, mail_admins

# Create your models here.


class Cliente(models.Model):
    nome = models.CharField(max_length=255, verbose_name='Nome:')
    cpf = models.CharField(max_length=100, verbose_name='CPF:', unique=True, blank=True, null=True)
    email = models.EmailField(max_length=255, verbose_name='E-mail:')
    telefone = models.CharField(max_length=50, verbose_name='Telefone Fixo:')
    celular = models.CharField(max_length=50, verbose_name='Celular/WhatsApp:')
    cep = models.CharField(max_length=100)
    logradouro = models.CharField(max_length=300, verbose_name='Logradouro:')
    numero = models.CharField(max_length=30, verbose_name='Número:')
    bairro = models.CharField(max_length=100, verbose_name='Bairro:')
    complemento = models.CharField(max_length=255, blank=True, null=True, verbose_name="Complemento:")
    cidade = models.CharField(max_length=200, verbose_name='Cidade:')
    estado = models.CharField(max_length=50, verbose_name='Estado:')


    def __str__(self):
        return self.nome

    def get_absolute_url(self):
        return reverse('listar_cliente')