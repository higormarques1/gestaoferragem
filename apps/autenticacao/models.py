# -*- coding: utf-8 -*-
from rest_framework.authtoken.models import Token
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.db import models
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager, PermissionsMixin)



class PersonalizadorBaseUserManager(BaseUserManager):
    def create_user(self, usuario, password):
        user = self.model(usuario=usuario)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, usuario, password):
        user = self.create_user(usuario, password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Usuario(AbstractBaseUser, PermissionsMixin):

    usuario = models.CharField(max_length=100, unique=True, verbose_name='Matricula')
    nome = models.CharField(max_length=100, blank=True, null=True)
    cpf = models.CharField(max_length=14, blank=False, null=False, unique=True)
    telefone = models.CharField(max_length=14, blank=True, null=True)
    is_active = models.BooleanField(default=True, blank=True, null=True, verbose_name='Torna Usuario Ativo')
    is_staff = models.BooleanField(default=True, blank=True, null=True, verbose_name='Torna Usuario da Instituição')
    email = models.EmailField(max_length=100, blank=True, null=True)

    USERNAME_FIELD = 'usuario'

    objects = PersonalizadorBaseUserManager()

    def get_absolute_url(self):
        return reverse('listar_usuario')

    def get_full_name(self):
        return self.usuario

    def get_short_name(self):
        return self.usuario



