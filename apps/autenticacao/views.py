from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

# Create your views here.
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, ListView, UpdateView
from .models import Usuario
from django.urls import reverse_lazy
from .forms import UsuarioForm


def logar(request, template_name="acesso/login.html"): #lOGA NO SISTEMA E DIRECIONA PARA PAGINA ANTERIOR OU PARA INDEX
    next = request.GET.get('next', '/')
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        try:
            user = authenticate(username=username, password=password)
        except:
            messages.error(request, 'Usuário ou senha incorretos.')
            return redirect('logar')
        if user is not None:
            login(request, user)
            return redirect('index')
        else:
            messages.error(request, 'Usuário ou senha incorretos.')
            return redirect('logar')

    return render(request, template_name, {'redirect_to': next})


@login_required()
def deslogar(request):  # DESLOGA DO SISTEMA E DIRECIONA PARA A PAGINA DE LOGIN
    logout(request)
    return HttpResponseRedirect(settings.LOGIN_URL)


@method_decorator([login_required], name='dispatch')
class CreateUsuario(CreateView):
    model = Usuario
    template_name = 'usuario/cadastro.html'
    success_url = reverse_lazy('listar_usuario')
    form_class = UsuarioForm


@method_decorator([login_required], name='dispatch')
class ListUsuario(ListView):
    model = Usuario
    template_name = 'usuario/listar.html'

    def get_queryset(self):
        return Usuario.objects.all()


@method_decorator([login_required], name='dispatch')
class UpdateUsuario(UpdateView):
    model = Usuario
    template_name = 'usuario/atualizar.html'
    fields = ['usuario', 'nome', 'is_active', 'is_staff', 'email', 'tipo_usuario']

