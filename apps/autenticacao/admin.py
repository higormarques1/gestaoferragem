from django.contrib import admin
from .models import Usuario
from django.contrib.auth.admin import UserAdmin


class PersonalizadorUserAdmin(UserAdmin):
    fieldsets = ()

    add_fieldsets = (
        (None, {
            'fields': ('usuario', 'password1', 'password2')
        }),

    )
    list_display = ('usuario', 'is_active', 'is_staff',)
    search_fields = ('usuario',)
    ordering = ('usuario',)


admin.site.register(Usuario, PersonalizadorUserAdmin)