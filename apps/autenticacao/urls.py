from django.urls import path
from .views import *
from django.contrib.auth import views as auth_views



urlpatterns = [
    path('logar/', logar, name='logar'),
    path('deslogar/', deslogar, name='deslogar'),
    path('usuario/cadastro/', CreateUsuario.as_view(), name='cadastro_usuario'),
    path('usuario/listar/', ListUsuario.as_view(), name='listar_usuario'),
    path('usuario/atualizar/<int:pk>/', UpdateUsuario.as_view(), name='atualizar_usuario'),
]
