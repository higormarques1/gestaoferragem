from datetime import datetime

from apps.estoque.models import Estoque


class Util:

    def geradorcodigovenda():
        ano = datetime.today().year
        saida = Estoque.objects.last()
        if saida == None:
            prox_codigo = 1
        else:
            prox_codigo = saida.id + 1

        cod_venda = int("%s%06d" % (ano, prox_codigo))


        return cod_venda

