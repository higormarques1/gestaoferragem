from django.db import models
import datetime

from django.urls import reverse
from apps.autenticacao.models import Usuario
from apps.cliente.models import Cliente
from apps.estoque.managers import EstoqueEntradaManager, EstoqueSaidaManager
from apps.material.models import MaterialEstoque


MOVIMENTO_CHOICES = (
    ('e', 'Entrada'),
    ('s', 'Saída'),
)

SITUACAO_COICHES = (
    ('pg', 'PAGO'),
    ('pend', 'PENDENTE')
)

SITUACAO_ENTREGA = (
    ('f', 'FINALIZADA'),
    ('p', 'PENDENTE')
)



class Estoque(models.Model):
    codigo = models.CharField(max_length=50, null=True, blank=True)
    data = models.DateTimeField(verbose_name=(
        'Data da Entrada:'), blank=True, null=True)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, verbose_name='Funcionário:')
    movimento = models.CharField(max_length=50, choices=MOVIMENTO_CHOICES, blank=True, verbose_name='Tipo de Evento:')
    valor_total = models.FloatField(blank=True, null=True, default=0)
    desconto = models.FloatField(blank=True, null=True, default=0)
    situacao = models.CharField(max_length=10, choices=SITUACAO_COICHES, verbose_name='Situação:')
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE, null=True, blank=True)
    entrega = models.BooleanField(blank=True, null=True, default=False)
    status_entrega = models.CharField(max_length=30, blank=True, null=True, choices=SITUACAO_ENTREGA, default='p')

    def get_absolute_url(self):
        return reverse('listar_material')

    def __str__(self):
        return self.codigo


class ItensEstoque(models.Model):
    estoque = models.ForeignKey(Estoque, on_delete=models.CASCADE, related_name='estoques')
    material = models.ForeignKey(MaterialEstoque, on_delete=models.CASCADE)
    quantidade = models.FloatField(default=0)
    saldo = models.FloatField(blank=True, null=True)
    valor = models.FloatField(blank=True, null=True)

    def get_absolute_url(self):
        return reverse('listar_material')


class EstoqueEntrada(Estoque):

    objects = EstoqueEntradaManager()
    class Meta:
        proxy = True


class EstoqueSaida(Estoque):
    objects = EstoqueSaidaManager()

    class Meta:
        proxy = True