from django.contrib.auth.decorators import login_required
from django.forms import inlineformset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render, resolve_url, redirect

# Create your views here.
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView

from apps.cliente.models import Cliente
from apps.estoque.forms import EstoqueEntradaForm, EstoqueSaidaForm, \
    FormItensEstoqueSaida, FormItensEstoqueEntrada
from apps.estoque.models import MaterialEstoque, Estoque, ItensEstoque, EstoqueEntrada, EstoqueSaida
from apps.estoque.uteis import Util


def dar_baixa_estoque(form):
    materiais = form.estoques.all()
    for item in materiais:
        material = MaterialEstoque.objects.get(pk=item.material.pk)
        material.estoque = item.saldo
        material.save()
    print('Estoque Atualizado com sucesso.')


@method_decorator([login_required], name='dispatch')
class ListEntradaEstoque(ListView):
    model = EstoqueEntrada
    template_name = "listar_entrada_estoque.html"
    context_object_name = "object_list"
    paginate_by = 10


#Função de Entrada de Estoque
def estoque_add(request, template_name, movimento, url):
    estoque_form = Estoque()
    item_estoque_formset = inlineformset_factory(Estoque, ItensEstoque,
                                                 fields=['material', 'quantidade', 'saldo', 'valor'],
                                                 form=FormItensEstoqueEntrada, extra=1)
    if request.method == 'POST':
        form = EstoqueEntradaForm(request.POST, instance=estoque_form, prefix='main')
        formset = item_estoque_formset(request.POST, instance=estoque_form, prefix='estoque')
        if form.is_valid() and formset.is_valid():
            form = form.save(commit=False)
            form.movimento = movimento
            form.save()
            formset.save()
            dar_baixa_estoque(form)
            return {'pk': form.pk}
    else:
        form = EstoqueEntradaForm(instance=estoque_form, prefix='main')
        formset = item_estoque_formset(instance=estoque_form, prefix='estoque')
    context = {'form': form, 'formset': formset, 'codigoentrada': Util.geradorcodigovenda()}
    return context


@login_required
def entrada_estoque(request):
    template_name = "entrada_estoque.html"
    movimento = 'e'
    url = 'listar_entrada_estoque'
    context = estoque_add(request, template_name, movimento, url)
    if context.get('pk'):
        return HttpResponseRedirect(resolve_url(url))
    return render(request, template_name, context)


@method_decorator([login_required], name='dispatch')
class ListSaidaEstoque(ListView):
    model = EstoqueSaida
    template_name = "listar_saida_estoque.html"
    context_object_name = "object_list"
    paginate_by = 10

    def get_queryset(self):
        return EstoqueSaida.objects.all().order_by('-codigo')


#Função de Saida de Estoque
def saida_add(request, template_name, movimento, url):
    estoque_form = Estoque()
    item_estoque_formset = inlineformset_factory(Estoque, ItensEstoque,
                                                 fields=['estoque', 'material', 'quantidade', 'saldo', 'valor'],
                                                 form=FormItensEstoqueSaida, extra=1)
    if request.method == 'POST':
        form = EstoqueSaidaForm(request.POST, instance=estoque_form, prefix='main')
        formset = item_estoque_formset(request.POST, instance=estoque_form, prefix='estoque')
        if form.is_valid() and formset.is_valid():
            form = form.save(commit=False)
            form.movimento = movimento
            form.save()
            formset.save()
            dar_baixa_estoque(form)
            return {'pk': form.pk}
    else:
        form = EstoqueSaidaForm(instance=estoque_form, prefix='main')
        formset = item_estoque_formset(instance=estoque_form, prefix='estoque')
    context = {'form': form, 'formset': formset, 'codigovenda': Util.geradorcodigovenda()}
    return context


@login_required
def saida_estoque(request):
    template_name = "saida_estoque.html"
    movimento = 's'
    url = 'index'
    context = saida_add(request, template_name, movimento, url)
    if context.get('pk'):
        return HttpResponseRedirect(resolve_url(url))
    return render(request, template_name, context)


@method_decorator([login_required], name='dispatch')
class EstoqueDetail(DetailView):
    model = Estoque
    template_name = 'detalhar_entrada_saida.html'


@login_required()
def pagamento_pendente(request, pk):
    es = EstoqueSaida.objects.get(pk=pk)
    es_form = EstoqueSaidaForm(request.POST or None, instance=es)
    if es_form.is_valid():
        es_form.save()
        return redirect(reverse('listar_venda'))
    return render(request, 'atualizar_venda.html', {
        'es_form': es_form,
        'es': es,
    })


@login_required()
def listar_venda_pendente(request):
    venda = EstoqueSaida.objects.all()
    cliente = Cliente.objects.all()
    return render(request, 'listar_venda_pendente.html', {'cliente': cliente, 'venda': venda})


@login_required()
def entrega_pendente(request, pk):
    es = EstoqueSaida.objects.get(pk=pk)
    cliente = Cliente.objects.filter(pk=es.cliente_id)
    es_form = EstoqueSaidaForm(request.POST or None, instance=es)
    if es_form.is_valid():
        es_form.save()
        return redirect(reverse('listar_entrega'))
    return render(request, 'atualizar_entrega.html', {
        'es_form': es_form,
        'es': es,
        'cliente': cliente
    })


@login_required()
def listar_entrega(request):
    cliente = Cliente.objects.all()
    venda = EstoqueSaida.objects.all()
    return render(request, 'listar_entrega_pendente.html', {'venda': venda, 'cliente': cliente})
