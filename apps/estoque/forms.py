from bootstrap_datepicker_plus import DateTimePickerInput
from django import forms

from apps.estoque.models import ItensEstoque, Estoque


class EstoqueEntradaForm(forms.ModelForm):

    data = forms.DateTimeField(widget=DateTimePickerInput(format='%d/%m/%Y %H:%M:%S'),
                                          label="Data da Entrada:")

    class Meta:
        model = Estoque
        fields = ('usuario', 'codigo', 'data', 'movimento', 'valor_total')
        widgets = {
            'valor_total': forms.TextInput(attrs={'class': 'form-control', 'required': 'true', 'readonly': 'readonly'}),
            'codigo': forms.TextInput(attrs={'class': 'form-control', 'required': 'true', 'readonly': 'readonly'}),
        }


class EstoqueSaidaForm(forms.ModelForm):

    data = forms.DateTimeField(widget=DateTimePickerInput(format='%d/%m/%Y %H:%M:%S'),
                                          label="Data da Entrada:")


    class Meta:
        model = Estoque
        fields = ('usuario', 'codigo', 'data', 'movimento', 'valor_total',
                  'desconto', 'situacao', 'cliente', 'entrega', 'status_entrega')
        widgets = {
            'codigo': forms.TextInput(attrs={'class': 'form-control', 'required': 'true', 'readonly': 'readonly'}),
            'valor_total': forms.TextInput(attrs={'required': 'true', 'readonly': 'readonly'}),
            'desconto': forms.NumberInput(attrs={'required': 'false'}),
            'cliente': forms.Select(attrs={'required': 'true'}),

            # 'data-live-search': 'true'
        }


class FormItensEstoqueEntrada(forms.ModelForm):

    class Meta:
        model = ItensEstoque
        fields = ['material', 'quantidade', 'saldo']
        exclude = ()
        widgets = {
            'material': forms.Select(attrs={'class': 'form-control', 'style': 'width:300px;', 'required': 'true'}),
            'quantidade': forms.NumberInput(attrs={'class': 'form-control', 'required': 'true'}),
            'valor': forms.TextInput(attrs={'class': 'form-control', 'required': 'true'}),
        }


class FormItensEstoqueSaida(forms.ModelForm):

    class Meta:
        model = ItensEstoque
        fields = ['material', 'quantidade', 'saldo', 'valor', 'estoque']
        exclude = ()
        widgets = {
            'material': forms.Select(attrs={'class': 'form-control', 'style': 'width:300px;', 'required': 'true'}),
            'quantidade': forms.NumberInput(attrs={'class': 'form-control', 'required': 'true'}),
            'valor': forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}),
        }