from django.contrib import admin

# Register your models here.
from apps.estoque.models import ItensEstoque, EstoqueEntrada, EstoqueSaida, Estoque


class ItemEntradaInline(admin.TabularInline):
    model = ItensEstoque
    extra = 1


class EntradaAdmin(admin.ModelAdmin):
    inlines = [ItemEntradaInline]


admin.site.register(EstoqueEntrada, EntradaAdmin)
admin.site.register(Estoque)
admin.site.register(EstoqueSaida, EntradaAdmin)