from django.urls import path, include
from apps.estoque.views import *


entrada_patterns = [
    path('listar/', ListEntradaEstoque.as_view(), name='listar_entrada_estoque'),
    path('', entrada_estoque, name='entrada_estoque'),
]

saida_patterns = [
    path('listar/', ListSaidaEstoque.as_view(), name='listar_saida_estoque'),
    path('', saida_estoque, name='saida_estoque'),
]

urlpatterns = [
    path('estoque/entrada/', include(entrada_patterns)),
    path('estoque/saida/', include(saida_patterns)),
    path('detalhar/<int:pk>/', EstoqueDetail.as_view(), name='detalhar_entrada_saida'),
    path('venda/pendente/atualizar/<int:pk>/', pagamento_pendente, name='atualizar_venda'),
    path('venda/pendente/', listar_venda_pendente, name='listar_venda'),
    path('entrega/pendente/atualizar/<int:pk>/', entrega_pendente, name='atualizar_entrega'),
    path('entrega/pendente/', listar_entrega, name='listar_entrega'),
]