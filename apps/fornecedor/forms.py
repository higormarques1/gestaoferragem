from bootstrap_datepicker_plus import DateTimePickerInput
from django import forms

from apps.fornecedor.models import EmpresaContratante


class EmpresaContratanteForm(forms.ModelForm):

    cnpj = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'required': 'true', 'data-mask': '00.000.000/0000-00',
               'placeholder': '00.000.000/0000-00'}), label="CNPJ")

    cpf = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'required': 'true', 'data-mask': '000.000.000-00',
               'placeholder': '000.000.000-00'}), label="CPF")

    telefone = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'required': 'true', 'data-mask': '(00) 0000-0000',
               'placeholder': '(00) 0000-0000'}), label="Telefone")

    celular = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'required': 'true', 'data-mask': '(00) 00000-0000',
               'placeholder': '(00) 00000-0000'}), label="Celular")

    cep = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'data-mask': '00000-000', 'placeholder': '00000-000'}),
        label='CEP')

    class Meta:
        model = EmpresaContratante
        fields = ['nome', 'cnpj', 'cpf', 'razao_social', 'email', 'telefone', 'celular',
                  'responsavel','cep', 'logradouro', 'numero', 'bairro', 'complemento', 'cidade',
                  'estado']