from django.urls import path
from apps.fornecedor.views import *

urlpatterns = [
    path('listar/', ListEmpContratante.as_view(), name='listar_empresa'),
    path('cadastrar/', CreateEmpContratante.as_view(), name='cadastrar_empresa'),
    path('atualizar/<int:pk>/', UpdateEmpContratante.as_view(), name='atualizar_empresa'),

]
