# Create your views here.
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, ListView, UpdateView

from apps.fornecedor.forms import EmpresaContratanteForm
from apps.fornecedor.models import EmpresaContratante


@method_decorator([login_required], name='dispatch')
class CreateEmpContratante(CreateView):
    model = EmpresaContratante
    form_class = EmpresaContratanteForm
    template_name = 'cadastrar_empresa.html'


@method_decorator([login_required], name='dispatch')
class ListEmpContratante(ListView):
    model = EmpresaContratante
    context_object_name = 'object_list'
    template_name = 'listar_empresa.html'
    paginate_by = 10


@method_decorator([login_required], name='dispatch')
class UpdateEmpContratante(UpdateView):
    model = EmpresaContratante
    form_class = EmpresaContratanteForm
    template_name = 'cadastrar_empresa.html'

    def get_object(self, queryset=None):
        return EmpresaContratante.objects.get(pk=self.kwargs['pk'])
