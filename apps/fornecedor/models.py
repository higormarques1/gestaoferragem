from django.db import models
import datetime

from django.urls import reverse


class EmpresaContratante(models.Model):
    nome = models.CharField(max_length=255, verbose_name='Nome Fantasia')
    cnpj = models.CharField(max_length=100, verbose_name='CNPJ')
    cpf = models.CharField(max_length=100, verbose_name='CPF')
    razao_social = models.CharField(max_length=100, verbose_name='Razão Social', null=True, blank=True)
    email = models.EmailField(max_length=255, verbose_name='E-mail', null=True, blank=True)
    telefone = models.CharField(max_length=50, verbose_name='Telefone Fixo', null=True, blank=True)
    celular = models.CharField(max_length=50, verbose_name='Celular/whatsapp', null=True, blank=True)
    responsavel = models.CharField(max_length=255, verbose_name='Responsável', null=True, blank=True)
    cep = models.CharField(max_length=100)
    logradouro = models.CharField(max_length=300, verbose_name='Logradouro')
    numero = models.CharField(max_length=30, verbose_name='Nº')
    bairro = models.CharField(max_length=100, verbose_name='Bairro')
    complemento = models.CharField(max_length=255, blank=True, null=True, verbose_name="Complemento")
    cidade = models.CharField(max_length=200, verbose_name='Cidade')
    estado = models.CharField(max_length=50, verbose_name='Estado')

    def get_absolute_url(self):
        return reverse('listar_empresa')
