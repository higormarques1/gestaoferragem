from django.contrib import admin

# Register your models here.
from apps.fornecedor.models import EmpresaContratante

admin.site.register(EmpresaContratante)
