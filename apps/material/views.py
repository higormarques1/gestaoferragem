from bootstrap_modal_forms.generic import BSModalCreateView, BSModalUpdateView
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
# Create your views here.
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from apps.material.forms import FormMaterial
from apps.material.models import MaterialEstoque
from apps.material.uteis import Util


@method_decorator([login_required], name='dispatch')
class CreateMaterial(BSModalCreateView):
    success_url = reverse_lazy('listar_material')
    form_class = FormMaterial
    template_name = "cadastrar_material.html"

    def get_context_data(self, **kwargs):
        context = super(CreateMaterial, self).get_context_data(**kwargs)
        context['codigo_mat'] = Util.geradorcodigo()
        return context


@method_decorator([login_required], name='dispatch')
class UpdateMaterial(BSModalUpdateView):
    model = MaterialEstoque
    template_name = 'atualizar_material.html'
    form_class = FormMaterial
    success_url = reverse_lazy('listar_material')



@method_decorator([login_required], name='dispatch')
class ListMaterial(ListView):
    model = MaterialEstoque
    template_name = "listar_material.html"
    context_object_name = "object_list"
    paginate_by = 10



@login_required()
def material_json(request, pk):
    ''' Retorna o material, id e estoque. '''
    material = MaterialEstoque.objects.filter(pk=pk)
    data = [item.to_dict_json() for item in material]
    return JsonResponse({'data': data})

