from bootstrap_modal_forms.forms import BSModalForm
from django import forms

from apps.material.models import MaterialEstoque


class FormMaterial(BSModalForm):
    class Meta:
        model = MaterialEstoque
        fields = ('codigo', 'nome', 'estoque', 'preco', 'estoque_minima', 'fornecedor')
        widgets = {
            'codigo':forms.TextInput(attrs={'class': 'form-control', 'required': 'false', 'readonly': 'readonly'}),
            'estoque': forms.TextInput(attrs={'class': 'form-control', 'required': 'false', 'readonly': 'readonly'})
        }