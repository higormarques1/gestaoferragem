from apps.material.models import MaterialEstoque


class Util:

    def geradorcodigo():
        mat = MaterialEstoque.objects.last()
        if mat == None:
            prox_codigo = 1
        else:
            prox_codigo = mat.id + 1

        cod_material = "3210%06d" % (prox_codigo)
        return cod_material


