from django.urls import path
from apps.material.views import material_json, ListMaterial, CreateMaterial, UpdateMaterial

urlpatterns = [
    path('', ListMaterial.as_view(), name='listar_material'),
    path('cadastrar/', CreateMaterial.as_view(), name='cadastrar_material'),
    path('atualizar/<int:pk>', UpdateMaterial.as_view(), name='atualizar_material'),
    path('<int:pk>/json/', material_json, name='material_json'),
]