from django.db import models
from django.urls import reverse

from apps.fornecedor.models import EmpresaContratante


class MaterialEstoque(models.Model):
    codigo = models.CharField(max_length=100, unique=True)
    nome = models.CharField(max_length=100, verbose_name="Descrição")
    estoque = models.FloatField(default=0, verbose_name="Quantidade")
    preco = models.DecimalField(max_digits=6, decimal_places=2, default=0.0, verbose_name="Preço de Venda")
    estoque_minima = models.FloatField(default=0, verbose_name="Quantidade Minima")
    fornecedor = models.ForeignKey(EmpresaContratante, on_delete=models.CASCADE, blank=True, null=True)

    def to_dict_json(self):
        return {
            'pk': self.pk,
            'material': self.nome,
            'estoque': self.estoque,
            'preco': self.preco,
        }

    def __str__(self):
        return self.codigo + ' - ' + self.nome + ' -  R$ ' + str(self.preco)

    def get_absolute_url(self):
        return reverse('listar_material')
