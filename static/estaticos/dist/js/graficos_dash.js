var endpoint = "/api/venda/"
var totalVenda = []
var totalEntrada = []
var totalMesAtual = []
var lucro = []
var jan = []
var fev = []
var mar = []
var abr = []
var mai = []
var jun = []
var jul = []
var ago = []
var set = []
var out = []
var nov = []
var dez = []

var mat
var val
var itement

$.ajax({
    type: "GET",
    url: endpoint,
    success: function(data){
        totalVenda = data.tot
        totalEntrada = data.totent
        lucro = data.lucro
        totalMesAtual = data.totmes
        item = data.item
        jan = data.jan
        fev = data.fev
        mar = data.mar
        abr = data.abr
        mai = data.mai
        jun = data.jun
        jul = data.jul
        ago = data.ago
        set = data.set
        out = data.out
        nov = data.nov
        dez = data.dez

        itement = data.quant_entrada
        itemsai = data.quant_saida

        mat = Object.keys(item)
        val = Object.values(item)

        var chart =  setInterval(lucro, 3000);
        setChart();

        setQuatidade();
    }
});

function setQuatidade(){
    var ent = document.getElementById("entrada")
    var sai = document.getElementById("saida")

    var novoValorEntrada = itement
    var novoValorSaida = itemsai

    ent.innerHTML = novoValorEntrada
    sai.innerHTML = novoValorSaida


}

function setChart(){
    var ctx = document.getElementById("totalvendas");
    var ctx2 = document.getElementById("totalmes");
    var ctx3 = document.getElementById("matvenda");
    var ctx4 = document.getElementById("totalmesatual");
    var productsChart = new Chart(ctx2, {
        type: 'line',
        data: {
            labels: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro",
                        "Novembro", "Dezembro"],
            datasets: [{

                label: "",
                data: [jan, fev, mar, abr, mai, jun , jul, ago, set, out, nov, dez],
                backgroundColor: [
                    'rgba(0, 0, 255, 0.5)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',
                    'rgba(255, 255, 255, 1)',


                ],
                borderColor: [
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',

                ],
                borderWidth: 1,
            }],
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                    }
                }]
            }
        }
    });
    var productsChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Total de Entrada", "Total de Vendas", "Lucro"],
            datasets: [{
                label: "",
                data: [totalEntrada, totalVenda, lucro],
                backgroundColor: [
                    'rgba(27, 26, 231, 1)',
                    'rgba(69, 232, 75, 1)',
                    'rgba(255, 255, 0, 1)',
                ],
                borderColor: [
                    'rgba(27, 26, 231, 1)',
                    'rgba(69, 232, 75, 1)',
                    'rgba(255, 255, 0, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    var productsChart = new Chart(ctx3, {
        type: 'horizontalBar',
        data: {
            labels: mat,
            datasets: [{
                label: "",
                data: val,
                backgroundColor: [
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                ],
                borderColor: [
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                    'rgba(27, 26, 231, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    var productsChart = new Chart(ctx4, {
        type: 'doughnut',
        data: {
            labels: [''],
            datasets: [{
                label: "Total do mês atual",
                data: [totalMesAtual],
                backgroundColor: [
                    'rgba(27, 26, 231, 0.9)',
                ],
                borderColor: [
                    'rgba(255, 255, 255, 1)',
                ],
                borderWidth: 2
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}