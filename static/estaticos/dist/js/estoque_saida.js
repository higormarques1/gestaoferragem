$(document).ready(function() {
  // Insere classe no primeiro item de produto
  $('#id_estoque-0-material').addClass('clProduto');
  $('#id_estoque-0-quantidade').addClass('clQuantidade');
  $('#id_estoque-0-desconto').addClass('clDesconto');
  $('#id_estoque-0-valor').addClass('clValor');

  $('#saldo').attr('id', 'estoque-0-saldo');
  $('#valor').attr('id', 'estoque-0-valor');

  // Desabilita o primeiro campo 'Saldo'
  $('#id_estoque-0-saldo').prop('type', 'hidden');
  // Cria um span para mostrar o saldo na tela.
  $('td[id="estoque-0-saldo"]').append('<span id="id_estoque-0-saldo-span" class="lead" style="padding-left:10px;" ></span>');
  $('td[id="estoque-0-saldo"]').append('<input id="id_estoque-0-inicial" class="form_control" type="hidden" />');
  $('td[id="estoque-0-valor"]').append('<input id="id_estoque-0-valor-material" class="form_control" type="hidden" />');
  // Select2
});


  $('#add-item').click(function(ev) {
    ev.preventDefault();
    var count = $('#estoque').children().length;
    var tmplMarkup = $('#item-estoque').html();
    var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);
    $('div#estoque').append(compiledTmpl);

    // update form count
    $('#id_estoque-TOTAL_FORMS').attr('value', count + 1);

    $('#saldo2').attr('id', 'estoque-' + (count) + '-saldo');
    $('#valor2').attr('id', 'estoque-' + (count) + '-valor');
    // Desabilita o campo 'saldo'
    $('#id_estoque-' + (count) + '-saldo').prop('type', 'hidden')

    // some animate to scroll to view our new form
    $('html, body').animate({
      scrollTop: $("#add-item").position().top + 50
    }, 900);

    $('#id_estoque-' + (count) + '-material').addClass('clProduto');
    $('#id_estoque-' + (count) + '-quantidade').addClass('clQuantidade');
    $('#id_estoque-' + (count) + '-desconto').addClass('clDesconto');
    $('#id_estoque-' + (count) + '-valor').addClass('clValor');


    // Cria um span para mostrar o saldo na tela.
    $('td[id="estoque-' + (count) + '-saldo"]').append('<span id="id_estoque-' + (count) + '-saldo-span" class="lead" style="padding-left:10px"></span>')
    // Cria um campo com o estoque inicial.
    $('td[id="estoque-' + (count) + '-saldo"]').append('<input id="id_estoque-' + (count) + '-inicial" class="form-control" type="hidden" />')
    // Cria um campo que vai receber o valor do material.
    $('td[id="estoque-' + (count) + '-valor"]').append('<input id="id_estoque-' + (count) + '-valor-material" class="form_control" type="hidden" />');
    // Select2
//    $('.clProduto').select2()
  });

let estoque
let saldo
let campo
let camp2
let quantidade
let estoque_inicial
let campo_estoque_inicial
let campo_valor

$(document).on('change', '.clProduto', function() {
  let self = $(this)
  let pk = $(this).val()
  let url = '/material/' + pk + '/json/'

  $.ajax({
    url: url,
    type: 'GET',
    success: function(response) {
      estoque = response.data[0].estoque
      preco = response.data[0].preco
      campo = self.attr('id').replace('material', 'quantidade')
      estoque_inicial = self.attr('id').replace('material', 'inicial')
      //Obter o id do input valor_material
      valor_material = self.attr('id').replace('material', 'valor-material')
      // Estoque inicial.
      $('#'+estoque_inicial).val(estoque)
      //Atribuir o valor do material ao input valor_material
      $('#'+valor_material).val(preco)
      // Remove o valor do campo 'quantidade'
      $('#'+campo).val('')
    },
    error: function(xhr) {
      // body...
    }
  })
});

let valor_total
let class_valor
let cv

//Responsavel por realizar verificação e calculo da quantidade com preço do material e
//da baixa no estoque do material.
$(document).on('keyup', '.clQuantidade', function() {
  quantidade = $(this).val();

  // Aqui é feito o cálculo de subtração do estoque
  campo = $(this).attr('id').replace('quantidade', 'saldo')

  var campo_estoque_inicial = $(this).attr('id').replace('quantidade', 'inicial')
  var campo_valor_material = $(this).attr('id').replace('quantidade', 'valor-material')

  estoque_inicial = $('#'+campo_estoque_inicial).val()
  valor_material = $('#'+campo_valor_material).val()

  saldo = Number(estoque_inicial) - Number(quantidade)
  // Atribui o saldo ao campo 'saldo'
  $('#'+campo).val(saldo)
  campo2 = $(this).attr('id').replace('quantidade', 'saldo-span')
  // Atribui o saldo ao campo 'id_estoque-x-saldo-span'
  $('#'+campo2).text(saldo)

  valor = parseFloat(quantidade * valor_material)
  campo_valor = $(this).attr('id').replace('quantidade', 'valor')
  //Adicionando o valor ao campo valor total
  $('#'+campo_valor).val((valor).toFixed(2));

  if (saldo < 0 ){
    Swal.fire({
        title: '',
        text: 'Quantidade acima do estoque!',
        type: 'warning',
        confirmButtonText: 'OK'
    });
    var qcampo = $(this).attr('id').replace('quantidade', 'quantidade');
    $('#'+campo).val('');
    $('#'+qcampo).val('');
    $('#'+campo_valor).val(0);
    $('#'+campo2).text(estoque_inicial);
  }


  class_valor = $('#'+campo_valor).attr('class');
  cv = class_valor.substring(class_valor.indexOf(" ") + 1);


});

//Função responsavel por realizar o calculo da somar todos os valor do input valor com também o desconto.
var vt =  setInterval(valort, 100);
function valort(){

    valor_total = 0;
    $('.'+cv).each(function(){
        var v = Number($(this).val());
        valor_total += v;
    });
    $("#id_main-valor_total").val(valor_total.toFixed(2));

    var v2 = $("#id_main-valor_total").val();
    var d = $("#id_main-desconto").val();
    valor_total = parseFloat(v2 - d);
    if(valor_total < 0){
        Swal.fire({
            title: '',
            text: 'Valor do desconto maior que valor total da venda!',
            type: 'warning',
            confirmButtonText: 'OK'
        });
        $("#id_main-desconto").val(0);

    }
    $("#id_main-valor_total").val(valor_total.toFixed(2));

}

//Função Responsavel por remover elemento da lista adicionado
(function($){
    remove = function(item) {
    var count = $('#estoque').children().length;
    var tr = $(item).closest('div');
//    console.log(tr)
    tr.fadeOut(50, function() {
        tr.remove();
        $('#id_estoque-TOTAL_FORMS').attr('value', count - 1);
    });
    return false;
  }
})(jQuery);


//Função Responsavel por validar o select e não deixa que o mesmo item seja escolhido.
var vs =  setInterval(validarSelect, 100);
function validarSelect(){
    var selects = document.getElementsByClassName("clProduto");

    for (var i = 0; i < selects.length; i++) {
        selects[i].onchange = function(e) {
            var val = this.value;

            for (var z = 0; z < selects.length; z++) {
                var index = Array.prototype.indexOf.call(selects, this);

                if ((z !== index) && selects[z].value === val) {
                    Swal.fire({
                        title: '',
                        text: 'Este material já foi selecionado.',
                        type: 'warning',
                        confirmButtonText: 'OK'
                    });
                    var options = this.getElementsByTagName("option");

                    for (var o = 0; o < options.length; o++) {
                        if (options[o].selected) {
                            options[o].selected = false;
                        }
                    }

                    options[0].selected = true;

                    return false;
                }
            }
        }
    }
};