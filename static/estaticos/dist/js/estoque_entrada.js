$(document).ready(function() {
  // Insere classe no primeiro item de produto
  $('#id_estoque-0-material').addClass('clProduto');
  $('#id_estoque-0-quantidade').addClass('clQuantidade');
  $('#id_estoque-0-valor').addClass('clValor');

  $('#saldo').attr('id', 'estoque-0-saldo');
  // Desabilita o primeiro campo 'Saldo'
  $('#id_estoque-0-saldo').prop('type', 'hidden')
  // Cria um span para mostrar o saldo na tela.
  $('td[id="estoque-0-saldo"]').append('<span id="id_estoque-0-saldo-span" class="lead" style="padding-left:10px;"></span>')
  $('td[id="estoque-0-saldo"]').append('<input id="id_estoque-0-inicial" class="form-control" type="hidden" />')

  // Select2
// $('.clProduto').select2()
});

  $('#add-item').click(function(ev) {
    ev.preventDefault();
    var count = $('#estoque').children().length;
    var tmplMarkup = $('#item-estoque').html();
    var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);
    $('div#estoque').append(compiledTmpl);

    // update form count
    $('#id_estoque-TOTAL_FORMS').attr('value', count + 1);

    $('#saldo2').attr('id', 'estoque-' + (count) + '-saldo');
    // Desabilita o campo 'Saldo'
    $('#id_estoque-' + (count) + '-saldo').prop('type', 'hidden');

    // some animate to scroll to view our new form
    $('html, body').animate({
      scrollTop: $("#add-item").position().top + 50
    }, 800);

    $('#id_estoque-' + (count) + '-material').addClass('clProduto');
    $('#id_estoque-' + (count) + '-quantidade').addClass('clQuantidade');
    $('#id_estoque-' + (count) + '-valor').addClass('clValor');

    // Cria um span para mostrar o saldo na tela.
    $('td[id="estoque-' + (count) + '-saldo"]').append('<span id="id_estoque-' + (count) + '-saldo-span" class="lead" style="padding-left: 10px;"></span>')
    $('td[id="estoque-' + (count) + '-saldo"]').append('<input id="id_estoque-' + (count) + '-inicial" class="form-control" type="hidden" />')
    // Select2
    //$('.clProduto').select2()
  });

let estoque
let saldo
let campo
let campo2
let quantidade


$(document).on('change', '.clProduto', function() {
  let self = $(this)
  let pk = $(this).val()
  let url = '/material/' + pk + '/json/'

  $.ajax({
    url: url,
    type: 'GET',
    success: function(response) {
      estoque = response.data[0].estoque
      campo = self.attr('id').replace('material', 'quantidade')
      estoque_inicial = self.attr('id').replace('material', 'inicial')

      $('#'+estoque_inicial).val(estoque)
      // Remove o valor do campo 'quantidade'
      $('#'+campo).val('')
    },
    error: function(xhr) {
      // body...
    }
  })
});

let valor_total
let class_valor
let cv

$(document).on('change', '.clQuantidade', function() {
  quantidade = $(this).val();
  // Aqui é feito o cálculo de soma do estoque
  campo = $(this).attr('id').replace('quantidade', 'saldo')
  campo_estoque_inicial = $(this).attr('id').replace('quantidade', 'inicial')
  estoque_inicial = $('#'+campo_estoque_inicial).val()
  saldo = Number(quantidade) + Number(estoque_inicial)

  // Desabilita o 'Saldo'
  $('#'+campo).prop('type', 'hidden')
  // Atribui o saldo ao campo 'saldo'
  $('#'+campo).val(saldo)
  campo2 = $(this).attr('id').replace('quantidade', 'saldo-span')
  // Atrubui o saldo ao campo 'id_estoque-x-saldo-span'
  $('#'+campo2).text(saldo)


  campo_valor = $(this).attr('id').replace('quantidade', 'valor')
  class_valor = $('#'+campo_valor).attr('class');
  cv = class_valor.substring(class_valor.indexOf(" ") + 1);


});


(function($){
    remove = function(item) {
    var count = $('#estoque').children().length;
    var tr = $(item).closest('div');
    tr.fadeOut(50, function() {
        tr.remove();
        $('#id_estoque-TOTAL_FORMS').attr('value', count - 1);
    });
    return false;
  }
})(jQuery);

var vs =  setInterval(validarSelect, 100);
function validarSelect(){
    var selects = document.getElementsByClassName("clProduto");

    for (var i = 0; i < selects.length; i++) {
        selects[i].onchange = function(e) {
            var val = this.value;

            for (var z = 0; z < selects.length; z++) {
                var index = Array.prototype.indexOf.call(selects, this);

                if ((z !== index) && selects[z].value === val) {
                    Swal.fire({
                        title: '',
                        text: 'Este material já foi selecionado.',
                        type: 'warning',
                        confirmButtonText: 'OK'
                    });
                    var options = this.getElementsByTagName("option");

                    for (var o = 0; o < options.length; o++) {
                        if (options[o].selected) {
                            options[o].selected = false;
                        }
                    }

                    options[0].selected = true;

                    return false;
                }
            }
        }
    }
};


var vt =  setInterval(valort, 100);
function valort(){

    valor_total = 0;
    $('.'+cv).each(function(){
        var v = Number($(this).val());
        valor_total += v;
    });
    $("#id_main-valor_total").val(valor_total.toFixed(2));

}